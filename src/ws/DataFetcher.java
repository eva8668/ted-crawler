package ws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DataFetcher {

	private static final String URL = "http://www.ted.com/talks?page=%d&sort=popular";

	public static void main(String[] args) {
		final int firstPage = 1;
		int lastPage = 1;
		try {
			// Find out the index of last page
			Document doc = Jsoup.connect(String.format(URL, firstPage)).get();
			String last = doc.select("div.results__pagination a.pagination__item").last().text();
			lastPage = Integer.parseInt(last);

			// Blank workbook
			XSSFWorkbook workbook = new XSSFWorkbook();

			// Create a blank sheet
			XSSFSheet sheet = workbook.createSheet("Popular talks");

			// CreationHelper createHelper = workbook.getCreationHelper();
			CellStyle hlink_style = workbook.createCellStyle();
			Font hlink_font = workbook.createFont();
			hlink_font.setUnderline(Font.U_SINGLE);
			hlink_font.setColor(IndexedColors.BLUE.getIndex());
			hlink_style.setFont(hlink_font);

			// Write Header
			int rownum = 0;
			int cellnum = 0;
			Row row = sheet.createRow(rownum++);
			row.createCell(cellnum++).setCellValue("Speaker");
			row.createCell(cellnum++).setCellValue("Title");
			row.createCell(cellnum++).setCellValue("Link");
			row.createCell(cellnum++).setCellValue("Popularity");
			row.createCell(cellnum++).setCellValue("Duration");
			row.createCell(cellnum++).setCellValue("Publish Date");

			Elements rows;
			String url;
			String speaker;
			String title;
			String link;
			String popularity;
			String duration;
			String date;
			Cell cell;
			// Hyperlink hLink;
			for (int i = firstPage; i <= lastPage; i++) {
				url = String.format(URL, i);
				System.err.println("[FETCH] " + url);

				doc = Jsoup.connect(url).get();
				rows = doc.select("div.results div.col div.media");

				for (Element ele : rows) {
					row = sheet.createRow(rownum++);
					cellnum = 0;

					// Speaker
					speaker = ele.select(".talk-link__speaker").get(0).text();
					row.createCell(cellnum++).setCellValue(speaker);

					// Title
					title = ele.select("h4.m5 a").get(0).text();
					row.createCell(cellnum++).setCellValue(title);

					// Link
					link = "http://www.ted.com" + ele.select("h4.m5 a").get(0).attr("href");
					cell = row.createCell(cellnum++);
					cell.setCellValue(link);
					// hLink = createHelper.createHyperlink(Hyperlink.LINK_URL);
					// hLink.setAddress(link);
					// cell.setHyperlink(hLink);
					// cell.setCellStyle(hlink_style);

					// Popularity
					popularity = ele.select("div.meta span.meta__val").get(0).text();
					row.createCell(cellnum++).setCellValue(popularity);

					// Duration
					duration = ele.select("span.thumb__duration").get(0).text();
					row.createCell(cellnum++).setCellValue(duration);

					// Publish Date
					date = ele.select("div.meta span.meta__val").get(1).text();
					row.createCell(cellnum++).setCellValue(date);

					// System.out.println("[TALK] " + speaker + ", " + title);
				}

				// Wait for sec in case of IP banned...
				Thread.sleep(1000);
			}

			// Write the workbook in file system
			FileOutputStream out = new FileOutputStream(new File("ted_popular_talks.xlsx"));
			workbook.write(out);
			workbook.close();
			out.close();

			System.err.println("[ted_popular_talks.xlsx] written successfully on disk.");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
